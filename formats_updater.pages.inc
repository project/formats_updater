<?php

/**
 * @file
 * Admin form API definitions.
 */

/**
 * The formats updater form.
 */
function formats_updater(&$form_state = array()) {
  // If we've got something in storage return the confirm form.
  if (!empty($form_state['storage'])) {
    return formats_updater_confirm_form($form_state);
  }

  $form = array();

  $form['from'] = array(
    '#type' => 'select',
    '#title' => t('From'),
    '#description' => t('Update nodes that currently have this input format...'),
    '#options' => formats_updater_formats(),
    '#required' => TRUE,
  );

  $form['to'] = array(
    '#type' => 'select',
    '#title' => t('To'),
    '#description' => t('...with this input format.'),
    '#options' => formats_updater_formats(),
    '#required' => TRUE,
  );

  $form['perform_access_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Access check'),
    '#description' => t('Prevent the input format of nodes being updated if the author does not have access to use the new input format.'),
    '#default_value' => TRUE,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter by user or role'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['options']['role'] = array(
    '#type' => 'select',
    '#title' => t('By role'),
    '#description' => t('Update the input format of nodes created by users with this role.'),
    '#options' => formats_updater_roles(),
  );

  $form['options']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('By user'),
    '#description' => t('Update the input format of nodes created by a specific user.'),
    '#maxlength' => 60,
    '#autocomplete_path'  => 'user/autocomplete',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Validate callback for the formats updater form.
 */
function formats_updater_validate($form, &$form_state) {
  // Check that the user has at least selected two formats before validating
  // the rest of the form.
  $failed = FALSE;
  if ($form_state['values']['from'] == 0) {
    form_set_error('from', t('From field is required.'));
    $failed = TRUE;
  }

  if ($form_state['values']['to'] == 0) {
    form_set_error('to', t('To field is required.'));
    $failed = TRUE;
  }

  if ($failed) {
    return;
  }

  // Good, validate the rest of the form.
  if ($form_state['values']['from'] === $form_state['values']['to']) {
    form_set_error('', t('You have selected the same input format for from and to, please change at least one selection.'));
  }

  $name = trim($form_state['values']['options']['name']);

  if ($form_state['values']['options']['role'] != 0 && !empty($name)) {
    form_set_error('options', t('You cannot select both a role and a user, please choose either a role or a user.'));
  }

  if (!empty($name) && !db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", $name))) {
    form_set_error('options][name', t('The user was not found, please start typing a username and select one from the list that appears.'));
  }
}

/**
 * Submit callback for the formats updater form.
 */
function formats_updater_submit($form, &$form_state) {
  // Store our values if we're not coming in from the confirm form.
  if (!isset($form_state['values']['confirm'])) {
    $form_state['storage'] = array('values' => $form_state['values']);
    return;
  }

  // Remove any whitespace around the username.
  $name = trim($form_state['values']['options']['name']);

  // Perform the operation appropriate to the options selected by the user.
  if (!empty($form_state['values']['options']['role'])) {
    formats_updater_update('role', $form_state['values']['from'], $form_state['values']['to'], $form_state['values']['options']['role'], $form_state['values']['perform_access_check']);
  }
  elseif (!empty($name)) {
    formats_updater_update('name', $form_state['values']['from'], $form_state['values']['to'], $form_state['values']['options']['name'], $form_state['values']['perform_access_check']);
  }
  else {
    formats_updater_update('all', $form_state['values']['from'], $form_state['values']['to'], NULL, $form_state['values']['perform_access_check']);
  }

  // We're done with our values.
  unset($form_state['storage']);
}
/**
 * Update confirmation confirm form.
 */
function formats_updater_confirm_form(&$form_state) {
  // Set our values in the form so they are carried through to the submit.
  $form['to'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['to'],
  );

  $form['from'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['from'],
  );

  $form['perform_access_check'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['perform_access_check'],
  );

  $form['options'] = array(
    '#type' => 'value',
    '#value' => $form_state['values']['options'],
  );

  // Remove any whitespace around the username.
  $name = trim($form_state['values']['options']['name']);

  // Present an appropriate question for the user to confirm or cancel.
  if (!empty($form_state['values']['options']['role'])) {
    $message = t('Are you sure you want to update the input format from %from to %to on all nodes authored by users with the %role role?', array('%from' => formats_updater_formats($form_state['values']['from']), '%to' => formats_updater_formats($form_state['values']['to']), '%role' => formats_updater_roles($form_state['values']['options']['role'])));
  }
  elseif (!empty($name)) {
    $message = t('Are you sure you want to update the input format from %from to %to on all nodes authored by user %name?', array('%from' => formats_updater_formats($form_state['values']['from']), '%to' => formats_updater_formats($form_state['values']['to']), '%name' => $form_state['values']['options']['name']));
  }
  else {
    $message = t('Are you sure you want to update the input formats from %from to %to for all nodes?', array('%from' => formats_updater_formats($form_state['values']['from']), '%to' => formats_updater_formats($form_state['values']['to'])));
  }

  return confirm_form($form, $message, 'admin/settings/filters/updater', NULL, t('Update formats'), t('Cancel'));
}

/**
 * Update the format column of the {node_revisions} table.
 *
 * @param $type
 *    (Optional) String, the type of update to perform, can be 'name', 'role'
 *    or 'all', defaults to 'all'.
 * @param $from
 *    Integer, the format to update.
 * @param $to
 *    Integer, the format to update to.
 * @param $arg
 *    (Optional) String or integer, depending on the value of $type, this will
 *    be a role ID if $type == 'role' or a username if $type == 'name' or not
 *    required if $type = 'all'.
 *  @param $perform_access_check
 *    (Optional) Boolean, TRUE to prevent the input format being updated to a
 *    format the author doesn't have access to, FALSE to update regardless,
 *    default to FALSE.
 *
 * @return
 *    Boolean, TRUE for success, FALSE on fail.
 */
function formats_updater_update($type = 'all', $from, $to, $arg = NULL, $perform_access_check = FALSE) {
  // Start with the basic query.
  $query = 'UPDATE {node_revisions} v ';

  // The set bit.
  $set = 'SET v.format = %d ';
  $arguments[] = $to;

  // And the format we want to update.
  $where[] = 'v.format = %d';
  $arguments[] = $from;

  // Add in role or username, depending on $type.
  switch ($type) {
    case 'name':
      if (!$account = user_load(array('name' => $arg))) {
        drupal_set_message(t('The user account could not be loaded.'));
        return FALSE;
      }
      $where[] = 'v.uid = %d';
      $arguments[] = $account->uid;
      break;

    case 'role':
      $where[] = 'v.uid IN (SELECT DISTINCT uid FROM {users_roles} WHERE rid = %d)';
      $arguments[] = $arg;
      break;
  }

  // Perform access check?
  $join = '';
  if ($perform_access_check) {
    $join = 'INNER JOIN {users_roles} r ON r.uid = v.uid ';
    $where[] = 'FIND_IN_SET (r.rid, (SELECT f.roles FROM {filter_formats} f WHERE f.format = %d))';
    $arguments[] = $to;
  }

  // Perform the update.
  $where = implode(' AND ', $where);
  $query .= $join . $set . ' WHERE ' . $where;

  timer_start('formats updater update');
  if (!db_query($query, $arguments)) {
    drupal_set_message(t('Unable to update the formats.'));
    return FALSE;
  }

  // Log the operation.
  $timer = timer_stop('formats updater update');
  global $user;
  watchdog('formats updater', 'Input formats changed from %from to %to by %name in !timems.', array('%from' => formats_updater_formats($from), '%to' => formats_updater_formats($to), '%name' => $user->name, '!time' => $timer['time']));

  // Report success, clear the cache and we're done.
  drupal_set_message(t('Formats successfully updated.'));
  cache_clear_all();

  return TRUE;
}

/**
 * Return an array of input formats for use as options.
 *
 * @param $format
 *    (Optional) Integer, the format to return.
 *
 * @return
 *    Array, an array containing either all the formats or a specific format if
 *    a $format was passed or an empty array if the format was not found.
 */
function formats_updater_formats($format = NULL) {
  static $formats;

  if (!isset($formats)) {
    $formats[] = t('Please select');
    $result = db_query('SELECT format, name FROM {filter_formats}');
    while ($row = db_fetch_object($result)) {
      $formats[$row->format] = $row->name;
    }
  }

  if ($format) {
    return isset($formats[$format]) ? $formats[$format] : array();
  }

  return $formats;
}

/**
 * Return an array of roles for use as options.
 *
 * @param $rid
 *    (Optional) Integer, the role ID to return.
 *
 * @return
 *    Array, an array containing either all the roles or a specific role if a
 *    $rid was passed or an empty array if the $rid was not found.
 */
function formats_updater_roles($rid = NULL) {
  static $roles;

  if (!isset($roles)) {
    $roles[] = t('All roles');
    $roles = $roles + user_roles(FALSE);
  }

  if ($rid) {
    return isset($roles[$rid]) ? $roles[$rid] : array();
  }

  return $roles;
}
