
CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Settings
  * How it works
  * Caution
  * Supported
  * The future
  * Author/Maintainer

INTRODUCTION
------------
The purpose of this module is to give site administrators the ability to change
the input format of nodes en-masse. In particular it helps to solve the issue
of users not being able to edit content they have created because the
administrator has inadvertently removed their permission to use the input
filter or has changed the input filter to something the author does not have
permission to use.

It can also help prevent the commonly asked question, "Where has my edit tab
gone?"

INSTALLATION
------------
Extract the formats_updater tarball to your modules directory, enable it at the
Administer -> Site building -> Modules page.

Assign trusted users the 'administer input formats updater' permission.

NB. This is an administrative tool that changes the input format of nodes,
ensure that you (and the users you grant this permission to) fully understand
the implications of changing input formats.

SETTINGS
--------
There is no settings page for this module, it simply adds a 'Formats updater'
tab to the Input formats page at Administer -> Site configuration -> Input
formats.

HOW IT WORKS
------------
This modules directly updates the format column of the node_revisions table. It
enables users with the 'administer input formats updater' permission to select
a format to update, it also allows further control over which content will be
updated giving a choice to filter by a particular user or role.

An additional option, 'Perform access check' ensures that formats are not
updated if the author of the node does not have permission to use the new
format, uncheck to 'force' formats to be updated regardless.

CAUTION
-------
Users who don't have permission to the input format set on a node will not have
access to edit that node regardless of other permission settings. As this
module enables administrators to directly manipulate this information it is
strongly recommended a backup of the database be taken beforehand. Used
incorrectly this module could easily lock content authors out of their content!

SUPPORTED
---------
This module has only been tested with PHP 5 and MySQL.

THE FUTURE
----------
For more information on what's happening in Drupal 7 around this issus see - 
http://drupal.org/node/110242

AUTHOR/MAINTAINER/CREDITS
-------------------------
matason - http://drupal.org/user/81488
This project has been sponsored by menusandblocks ltd, specialists in Drupal development and training.
Visit http://menusandblocks.co.uk for more information.
Thanks to siliconmeadow http://drupal.org/user/55284 for review and feedback.